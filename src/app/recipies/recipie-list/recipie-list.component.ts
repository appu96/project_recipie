import { Component, OnInit } from '@angular/core';
import { Recipies } from '../recipies.model';

@Component({
  selector: 'app-recipie-list',
  templateUrl: './recipie-list.component.html',
  styleUrls: ['./recipie-list.component.css']
})
export class RecipieListComponent implements OnInit {
  recipies: Recipies[] = [
    new Recipies('A Test recipies',
     'This is simply a test recipies',
     'https://www.pexels.com/photo/burrito-chicken-delicious-dinner-461198/'),
      new Recipies('A Test recipies',
     'This is simply a test recipies',
     'https://www.pexels.com/photo/burrito-chicken-delicious-dinner-461198/')
    ];

  constructor() { }

  ngOnInit() {
  }

}
