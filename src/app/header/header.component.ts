import { Component, EventEmitter } from '@angular/core';

import { from } from 'rxjs';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})


export class HeaderComponent {
  // @Output help to listen event from parent component i.e from app.html
  // @ts-ignore
  @Output() featureSelected = new EventEmitter<string>();
  onSelect(feature: string) {
   this.featureSelected.emit(feature); // feature is received as a string from html file onSelect method
  }
}

